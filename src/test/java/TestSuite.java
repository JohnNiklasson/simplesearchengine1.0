import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        MockDatabaseLoaderTest.class,
        SearchImplementationTest.class
})

public class TestSuite {
}
