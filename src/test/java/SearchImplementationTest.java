import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SearchImplementationTest {
    private MockDatabaseLoader mockDatabaseLoader;
    private SearchImplementation searchImplementation;

    @Before
    public void setUp() {
        mockDatabaseLoader = new MockDatabaseLoader();
        searchImplementation = new SearchImplementation();
    }

    @Test
    public void testFindArticles() {
        List<String> documents = new ArrayList<>();
        documents.add("All that is gold does not glitter");
        documents.add("Not all those who wander are lost");
        documents.add("The old that is strong does not wither");
        documents.add("Deep roots are not reached by the frost");
        Map<String, Map<String, Double>> mockDatabase = mockDatabaseLoader.createMockDatabaseFromInput(documents);
        assertEquals("Database should contain four entries",
                searchImplementation.findArticles("all not", mockDatabase, 4).size(),
                4);
    }
}
