import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MockDatabaseLoaderTest {

    private MockDatabaseLoader mockDatabaseLoader;

    @Before
    public void setUp() {
        mockDatabaseLoader = new MockDatabaseLoader();
    }

    @Test
    public void testCreateMockDatabase() {
        List<String> documentList = new ArrayList<>();
        documentList.add("All that is gold does not glitter");
        documentList.add("Not all those who wander are lost");
        documentList.add("The old that is strong does not wither");
        documentList.add("Deep roots are not reached by the frost");
        Map<String, Map<String, Double>> mockDatabase = mockDatabaseLoader.createMockDatabaseFromInput(documentList);
        assertTrue("Key 'gold' should have been created", mockDatabase.containsKey("gold"));
        assertTrue("Ket 'strong' should have been created", mockDatabase.containsKey("strong"));
    }

    @Test
    public void testCalculateTF() {

        String[] wordsArray = {"the", "lazy", "brown", "dog", "sat", "in", "the", "corner"};
        double tf = mockDatabaseLoader.calculateTF(wordsArray, "brown");

        assertEquals("Expected TF-IDF value 0.125 ", 0.125, tf, 0.00001);
    }
    @After
    public void tearDown() {
        mockDatabaseLoader = null;
    }
}
