import java.util.*;

import static java.util.stream.Collectors.toMap;

class SearchImplementation {
    Set<String> findArticles(String query, Map<String, Map<String, Double>> mockDatabase, double totalDocuments) {

        Map<String, Double> articles = new HashMap<>();
        String[] queryArray = query.split(" ");

        for (String searchWord : queryArray) {


            Map<String, Double> resultSet = mockDatabase.get(searchWord.toLowerCase());

            for (Map.Entry<String, Double> entry : resultSet.entrySet()) {

                if (articles.containsKey(entry.getKey())) {

                    articles.put(entry.getKey(),
                            calculateTFIDF(
                                    (entry.getValue() + articles.get(entry.getKey())),
                                    calculateIDF(totalDocuments, resultSet.size()))
                    );

                } else {

                    articles.put(entry.getKey(), entry.getValue() * calculateIDF(totalDocuments, resultSet.size()));
                }
            }
        }

        return sortByRelevancy(articles).keySet();

    }

    private double calculateIDF(double totalDocuments, double containingDocuments) {
        return totalDocuments / containingDocuments + 1;
    }

    private double calculateTFIDF(double termFrequency, double inverseDocumentFrequency) {
        return termFrequency * inverseDocumentFrequency;

    }

    private Map<String, Double> sortByRelevancy(Map<String, Double> articles) {

        return articles
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }
}
