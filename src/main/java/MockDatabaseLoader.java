import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MockDatabaseLoader {

    Map<String, Map<String, Double>> createMockDatabaseFromInput(List<String> documents) {


        Map<String, Map<String, Double>> mockDatabase = new HashMap<>();
        int documentCounter = 0;

        for (String sentence : documents) {

            documentCounter++;
            String[] wordArray = sentence
                    .toLowerCase()
                    .split(" ");

            for (String word : wordArray)
                if (mockDatabase.containsKey(word)) {

                    Map<String, Double> tempTFMap = mockDatabase.get(word);
                    tempTFMap.put("Document " + documentCounter, calculateTF(wordArray, word));
                    mockDatabase.put(word, tempTFMap);

                } else {

                    Map<String, Double> pathAndTFIDF = new HashMap<>();
                    pathAndTFIDF.put("Document " + documentCounter, calculateTF(wordArray, word));
                    mockDatabase.put(word, pathAndTFIDF);
                }
        }
        return mockDatabase;
    }

    double calculateTF(String[] documentContent, String term) {

        double termFrequency = 0;

        for (String word : documentContent) {
            if (word.equals(term)) {
                termFrequency++;
            }
        }

        termFrequency = termFrequency / documentContent.length;
        return termFrequency;
    }
}
