import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        SearchImplementation searchImplementation = new SearchImplementation();
        MockDatabaseLoader mockDatabaseLoader = new MockDatabaseLoader();

        List<String> documentArray = new ArrayList<>();

        System.out.print("Choose number of documents: ");
        int documents = scanner.nextInt();
        scanner.nextLine();

        for (int i = 1; i <= documents; i++) {

            System.out.print("Document " + i + ": ");
            documentArray.add("" + scanner.nextLine().toLowerCase());

        }

        Map<String, Map<String, Double>> mockDatabase = mockDatabaseLoader
                .createMockDatabaseFromInput(documentArray);

        System.out.println("Enter search query");
        String query = "" + scanner.nextLine();
        System.out.println("Result for query: " + query + " - " + searchImplementation.findArticles(query, mockDatabase, documents));
    }
}
